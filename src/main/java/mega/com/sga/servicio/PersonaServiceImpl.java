package mega.com.sga.servicio;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import mega.com.sga.domain.Persona;

@Stateless
public class PersonaServiceImpl implements PersonaServiceRemote{

	@Override
	public List<Persona> listarPersonas() {
		List<Persona> personas = new ArrayList<>();
		personas.add(new Persona(1, "Antonio", "Rodríguez", "omega@email.com", "123122333"));
		personas.add(new Persona(2, "Pepe", "López", "pepe@email.com", "3332221114"));
		return personas;
	}

	@Override
	public Persona encontrarPersonaPorId(int idPersona) {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public Persona encontrarPersonaPorEmail(String email) {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public void registrarPersona(Persona persona) {
		// TODO Apéndice de método generado automáticamente
		
	}

	@Override
	public void modificarPersona(Persona persona) {
		// TODO Apéndice de método generado automáticamente
		
	}

	@Override
	public void eliminarPersona(Persona persona) {
		// TODO Apéndice de método generado automáticamente
		
	}

}
