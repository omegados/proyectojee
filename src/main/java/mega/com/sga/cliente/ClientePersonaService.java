package mega.com.sga.cliente;

import java.util.List;
import java.util.Properties;

import javax.naming.*;

import mega.com.sga.servicio.PersonaServiceRemote;
import mega.com.sga.domain.Persona;

public class ClientePersonaService {
	public static void main(String[] args) {
		Properties properties = new Properties();
		properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
		
		System.out.println("Iniciando llamada al EJB desde el cliente\n");
		
		try {
			Context jndi = new InitialContext(properties);
			PersonaServiceRemote personaService = (PersonaServiceRemote) jndi.lookup("ejb:/sga-jee/PersonaServiceImpl!mega.com.sga.servicio.PersonaServiceRemote");
			List<Persona> personas = personaService.listarPersonas();
			for(Persona persona: personas) {
				System.out.println(persona);
			}
			System.out.println("Fin de nuestra llamada al EJB desde el cliente\n");
		} catch (NamingException e) {
			e.printStackTrace(System.out);
		}

		
	}

}
